package com.app.registration.secure.security.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.app.registration.secure.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;
	private int id;
	private String username;
	private String email;
	@JsonIgnore
	private String password;
	private int montantDepose;
	private int BTC;
	private int ETH;
	private int BNB;
	private int LTC;
	private int EOS;
	private int BCH;
	private int TRX;
	private int NEO;
	private int ADA;
	private int XRP;
	public UserDetailsImpl(int id, String email, String username, String password, int montantDepose, int bTC, int eTH, int bNB,
			int lTC, int eOS, int bCH, int tRX, int nEO, int aDA, int xRP) {
		this.id = id;
		this.email = email;
		this.username = username;
		this.password = password;
		this.montantDepose = montantDepose;
		BTC = bTC;
		ETH = eTH;
		BNB = bNB;
		LTC = lTC;
		EOS = eOS;
		BCH = bCH;
		TRX = tRX;
		NEO = nEO;
		ADA = aDA;
		XRP = xRP;
	}
	public static UserDetailsImpl build(User user) {
		return new UserDetailsImpl(
				user.getId(), 
				user.getUserName(), 
				user.getEmail(),
				user.getPassword(),
				user.getMontantDepose(),
				user.getADA(),
				user.getBTC(),
				user.getBCH(),
				user.getBNB(),
				user.getEOS(),
				user.getETH(),
				user.getLTC(),
				user.getNEO(),
				user.getTRX(),
				user.getXRP());
	}
	
	public int getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	@Override
	public String getPassword() {
		return password;
	}
	@Override
	public String getUsername() {
		return username;
	}
	public int getBTC() {
		return BTC;
	}
	public int getETH() {
		return ETH;
	}
	public int getBNB() {
		return BNB;
	}
	public int getLTC() {
		return LTC;
	}
	public int getEOS() {
		return EOS;
	}
	public int getBCH() {
		return BCH;
	}
	public int getTRX() {
		return TRX;
	}
	public int getNEO() {
		return NEO;
	}
	public int getADA() {
		return ADA;
	}
	public int getXRP() {
		return XRP;
	}
	public int getMontantDepose() {
		return montantDepose;
	}
	
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}
}