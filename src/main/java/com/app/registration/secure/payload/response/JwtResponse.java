package com.app.registration.secure.payload.response;

public class JwtResponse {

	private String token;
	private String type = "Bearer";
	private int id;
	private String email;
	private String username;
	private int montantDepose;
	
	private int BTC;
	private int ETH;
	private int BNB;
	private int LTC;
	private int EOS;
	private int BCH;
	private int TRX;
	private int NEO;
	private int ADA;
	private int XRP;
	
	public JwtResponse(String accessToken, int id, String email, String username,
			int montantDepose, int bTC, int eTH, int bNB, int lTC, int eOS, int bCH, int tRX, int nEO, int aDA,
			int xRP) {
		super();
		this.token = accessToken;
		this.id = id;
		this.email = email;
		this.username = username;
		this.montantDepose = montantDepose;
		BTC = bTC;
		ETH = eTH;
		BNB = bNB;
		LTC = lTC;
		EOS = eOS;
		BCH = bCH;
		TRX = tRX;
		NEO = nEO;
		ADA = aDA;
		XRP = xRP;
	}


	public String getAcessToken() {
		return token;
	}

	public void setAccessToken(String token) {
		this.token = token;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getMontantDepose() {
		return montantDepose;
	}

	public void setMontantDepose(int montantDepose) {
		this.montantDepose = montantDepose;
	}

	public int getBTC() {
		return BTC;
	}

	public void setBTC(int bTC) {
		BTC = bTC;
	}

	public int getETH() {
		return ETH;
	}

	public void setETH(int eTH) {
		ETH = eTH;
	}

	public int getBNB() {
		return BNB;
	}

	public void setBNB(int bNB) {
		BNB = bNB;
	}

	public int getLTC() {
		return LTC;
	}

	public void setLTC(int lTC) {
		LTC = lTC;
	}

	public int getEOS() {
		return EOS;
	}

	public void setEOS(int eOS) {
		EOS = eOS;
	}

	public int getBCH() {
		return BCH;
	}

	public void setBCH(int bCH) {
		BCH = bCH;
	}

	public int getTRX() {
		return TRX;
	}

	public void setTRX(int tRX) {
		TRX = tRX;
	}

	public int getNEO() {
		return NEO;
	}

	public void setNEO(int nEO) {
		NEO = nEO;
	}

	public int getADA() {
		return ADA;
	}

	public void setADA(int aDA) {
		ADA = aDA;
	}

	public int getXRP() {
		return XRP;
	}

	public void setXRP(int xRP) {
		XRP = xRP;
	}
	
	
	

}
