package com.app.registration.secure.payload.request;

import java.util.Objects;

public class SignupRequest {

	private int id;
	private String email;
	private String username;
	private String password;
	private int montantDepose;
	
	private int BTC;
	private int ETH;
	private int BNB;
	private int LTC;
	private int EOS;
	private int BCH;
	private int TRX;
	private int NEO;
	private int ADA;
	private int XRP;
	public SignupRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SignupRequest(int id, String email, String username, String password, int montantDepose, int bTC, int eTH,
			int bNB, int lTC, int eOS, int bCH, int tRX, int nEO, int aDA, int xRP) {
		super();
		this.id = id;
		this.email = email;
		this.username = username;
		this.password = password;
		this.montantDepose = montantDepose;
		BTC = bTC;
		ETH = eTH;
		BNB = bNB;
		LTC = lTC;
		EOS = eOS;
		BCH = bCH;
		TRX = tRX;
		NEO = nEO;
		ADA = aDA;
		XRP = xRP;
	}
	@Override
	public String toString() {
		return "LoginRequest [id=" + id + ", email=" + email + ", username=" + username + ", password=" + password
				+ ", montantDepose=" + montantDepose + ", BTC=" + BTC + ", ETH=" + ETH + ", BNB=" + BNB + ", LTC=" + LTC
				+ ", EOS=" + EOS + ", BCH=" + BCH + ", TRX=" + TRX + ", NEO=" + NEO + ", ADA=" + ADA + ", XRP=" + XRP
				+ "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(ADA, BCH, BNB, BTC, EOS, ETH, LTC, NEO, TRX, XRP, email, id, montantDepose, password,
				username);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SignupRequest other = (SignupRequest) obj;
		return ADA == other.ADA && BCH == other.BCH && BNB == other.BNB && BTC == other.BTC && EOS == other.EOS
				&& ETH == other.ETH && LTC == other.LTC && NEO == other.NEO && TRX == other.TRX && XRP == other.XRP
				&& Objects.equals(email, other.email) && id == other.id && montantDepose == other.montantDepose
				&& Objects.equals(password, other.password) && Objects.equals(username, other.username);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getMontantDepose() {
		return montantDepose;
	}
	public void setMontantDepose(int montantDepose) {
		this.montantDepose = montantDepose;
	}
	public int getBTC() {
		return BTC;
	}
	public void setBTC(int bTC) {
		BTC = bTC;
	}
	public int getETH() {
		return ETH;
	}
	public void setETH(int eTH) {
		ETH = eTH;
	}
	public int getBNB() {
		return BNB;
	}
	public void setBNB(int bNB) {
		BNB = bNB;
	}
	public int getLTC() {
		return LTC;
	}
	public void setLTC(int lTC) {
		LTC = lTC;
	}
	public int getEOS() {
		return EOS;
	}
	public void setEOS(int eOS) {
		EOS = eOS;
	}
	public int getBCH() {
		return BCH;
	}
	public void setBCH(int bCH) {
		BCH = bCH;
	}
	public int getTRX() {
		return TRX;
	}
	public void setTRX(int tRX) {
		TRX = tRX;
	}
	public int getNEO() {
		return NEO;
	}
	public void setNEO(int nEO) {
		NEO = nEO;
	}
	public int getADA() {
		return ADA;
	}
	public void setADA(int aDA) {
		ADA = aDA;
	}
	public int getXRP() {
		return XRP;
	}
	public void setXRP(int xRP) {
		XRP = xRP;
	}
	
}
